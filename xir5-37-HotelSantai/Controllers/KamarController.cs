﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using xir5_37_HotelSantai.Models.EntityManager;
using xir5_37_HotelSantai.Models.ViewModel;

namespace xir5_37_HotelSantai.Controllers
{
    public class KamarController : Controller
    {
        // GET: Kamar
        public ActionResult AddKamar()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddKamar(KamarView kv)
        {
            if (ModelState.IsValid)
            {
                KamarManager KM = new KamarManager();
                KM.AddKamar(kv);
                return RedirectToAction("Welcome", "Home");
            }
            return View();
        }
        public ActionResult ManageKamarPartial(string status = "")
        {
            //if (User.Identity.IsAuthenticated)
            //{
            string loginName = User.Identity.Name;
            KamarManager KM = new KamarManager();
            KamarDataView KDV = new KamarDataView();
            KDV.KamarProfile = KM.GetKamarData();
            string message = string.Empty;
            if (status.Equals("update"))
                message = "Update Successful";
            else if (status.Equals("delete"))
                message = "Delete Successful";
            ViewBag.Message = message;
            return PartialView(KDV);
            //}
            // return RedirectToAction("Index", "Home");
        }
        public ActionResult UpdateKamarData(int kamarID,int customerID, string noKamar, string
       namaKamar, DateTime tanggal)
        {
            KamarView KV = new KamarView();
            KV.id_kamar = kamarID;
            KV.id_customer = customerID;
            KV.nama_kamar = namaKamar;
            KV.no_kamar = noKamar;
            KV.tanggal = tanggal;
            KamarManager KM = new KamarManager();
            KM.UpdateKamar(KV);
            return Json(new { success = true });
        }
        public ActionResult DeleteKamar(int kamarID)
        {
            KamarManager KM = new KamarManager();
            KM.DeleteKamar(kamarID);
            return Json(new { success = true });
        }
        public ActionResult Perubahan()
        {
            return View();
        }
    }
}