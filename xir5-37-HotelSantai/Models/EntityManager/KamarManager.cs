﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using xir5_37_HotelSantai.Models.DB;
using xir5_37_HotelSantai.Models.ViewModel;

namespace xir5_37_HotelSantai.Models.EntityManager
{
    public class KamarManager
    {
        public void AddKamar(KamarView kv)
        {
            using (db_hotelEntities db = new db_hotelEntities())
            {
                tb_kamar km = new tb_kamar();
                km.id_customer = kv.id_customer;
                km.nama_kamar = kv.nama_kamar;
                km.no_kamar = kv.no_kamar;
                km.tanggal = kv.tanggal;
                db.tb_kamar.Add(km);
                db.SaveChanges();
            }
        }
        public void UpdateKamar(KamarView kv)
        {
            using (db_hotelEntities db = new db_hotelEntities())
            {
                tb_kamar km = db.tb_kamar.Find(kv.id_kamar);
                km.id_customer = kv.id_customer;
                km.nama_kamar = kv.nama_kamar;
                km.no_kamar = kv.no_kamar;
                km.no_kamar = kv.no_kamar;
                km.tanggal = kv.tanggal;
                //db.kamars.Add(km);
                db.SaveChanges();
            }
        }
        public List<KamarView> GetKamarData()
        {
            using (db_hotelEntities db = new db_hotelEntities())
            {
                var kamar = db.tb_kamar.Select(o => new KamarView
                {
                    id_kamar = o.id_kamar,
                    id_customer = o.id_customer,
                    nama_kamar = o.nama_kamar,
                    no_kamar = o.no_kamar,
                    tanggal = o.tanggal,

                }).ToList();
                return kamar;
            }
        }
        public void DeleteKamar(int kamarID)
        {
            using (db_hotelEntities db = new db_hotelEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var Km = db.tb_kamar.Where(o => o.id_kamar == kamarID);
                        if (Km.Any())
                        {
                            db.tb_kamar.Remove(Km.FirstOrDefault());
                            db.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
        }
    }
}
