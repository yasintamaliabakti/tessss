﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace xir5_37_HotelSantai.Models.ViewModel
{
    public class KamarView
    {
        [Key]
        public int id_kamar { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Nama Kamar")]
        public int id_customer { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Id Customer")]
        public string nama_kamar { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Nomor Kamar")]
        public string no_kamar { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Tanggal")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? tanggal { get; set; }
    }
    public class KamarDataView
    {
        public IEnumerable<KamarView> KamarProfile { get; set; }
    }
}
